
import { Menu } from "antd";
import { NavLink, useLocation } from "react-router-dom";

export default function Sidebar({color}:{color: string}){
  const { pathname } = useLocation();
  const container = pathname.replace("/", "");

    return (
        <>
          <div className="brand">
            <span>New Dashboard</span>
          </div>
          <hr />
          <Menu theme="light" mode="inline">
            <Menu.Item key="1">
              <NavLink to="/Home">
                <span
                  className="icon"
                  style={{
                    background: container === "home" ? color : "",
                  }}
                  />
                <span className="label">Home</span>
              </NavLink>
            </Menu.Item>
            <Menu.Item key="2">
              <NavLink to="/post">
                <span
                  className="icon"
                  style={{
                    background: container === "post" ? color : "",
                  }}
                  />
                <span className="label">Post</span>
              </NavLink>
            </Menu.Item>
            <Menu.Item key="3">
              <NavLink to="/Profile">
                <span
                  className="icon"
                  style={{
                    background: container === "profile" ? color : "",
                  }}
                  />
                <span className="label">Profile</span>
              </NavLink>
            </Menu.Item>
          </Menu>
        </>
      );
}