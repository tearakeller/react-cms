import {
    Row,
    Col,
    Card,
  } from "antd";
import Wysiwyg from "../../Utils/wysiwyg";

  export default function Post(){
    return (
        <>
          <div className="tabled">
            <Row gutter={[24, 0]}>
              <Col xs="24" xl={24}>
                <Card
                  bordered={false}
                  className="criclebox tablespace mb-24"
                >
                < Wysiwyg />
                </Card>
              </Col>
            </Row>
          </div>
        </>
      );
    }
    