import { Route } from "react-router-dom";
import Post from "./Containers/Post";
import Main from "./Containers/Main"
import "antd/dist/antd.css";
import "./assets/main.css";
import "./assets/responsive.css";

export default function App() {
  return (
    <div className="App">
        <Main>
          <Route path="/post" element={<Post />} />
        </Main>
    </div>
  );
}