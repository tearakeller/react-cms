import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAlignRight, faAlignCenter, faAlignLeft } from '@fortawesome/free-solid-svg-icons'
import './styles.scss'

export default function Wysiwyg() {
    const[ role, setRole]  = useState(false)
  
    return(
        <div className="editor">
            <div className="directives">
                <button onClick={()=>setRole(role)} data-role='bold'>B</button>
                <button onClick={()=>setRole(role)} data-role='italic'>I</button>
                <button onClick={()=>setRole(role)} data-role='underline'>U</button>
                <button onClick={()=>setRole(role)} data-role='justifyleft'><FontAwesomeIcon icon={faAlignLeft} /></button>
                <button onClick={()=>setRole(role)} data-role='justifycenter'><FontAwesomeIcon icon={faAlignCenter} /></button>
                <button onClick={()=>setRole(role)} data-role='justifyright'><FontAwesomeIcon icon={faAlignRight} /></button>
            </div>
            <div className="content-area" contentEditable placeholder='enter text' />
        </div>
    )
  };